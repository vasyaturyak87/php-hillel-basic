<?php

abstract class Figure
{
    abstract public function getArea(): int|float;

    abstract public function getPerimeter(): int|float;
}


class Rectangle extends Figure
{
    public int|float $width;
    public int|float $heigh;

    public function __construct(int|float $width, int|float $heigh)
    {
        $this->SetterWidth($width);
        $this->SetterHeigh($heigh);
    }

    /**
     * @throws Exception
     */
    public function SetterWidth(int|float $width): void
    {

        if ($width <= 0) {
            throw new Exception('Value width can not be less then zero or equal to zero'. PHP_EOL);
        }
        $this->width = $width;
    }

    public function SetterHeigh(int|float $heigh): void
    {

        if ($heigh <= 0) {
            throw new Exception('Value heigh can not be less then zero or equal to zero'. PHP_EOL);
        }
        $this->heigh = $heigh;
    }

    public function GetterWidth(): int|float
    {
        return $this->width;
    }

    public function GetterHeigh(): int|float
    {
        return $this->heigh;
    }

    public function getArea(): int|float
    {
        return $this->heigh * $this->width;
    }

    public function getPerimeter(): int|float
    {
        return $this->heigh * 2 + $this->width * 2;
    }
}

class Circle extends Figure
{
    public int|float $radius;

    public function __construct(int|float $radius)
    {
        $this->Setter($radius);
    }

    /**
     * @throws Exception
     */
    public function Setter(int|float $radius): void
    {

        if ($radius <= 0) {
            throw new Exception('Radius can not be less then zero or equal to zero'. PHP_EOL);
        }
        $this->radius = $radius;
    }

    public function Getter(): int|float
    {
        return $this->radius;
    }

    public function getArea(): int|float
    {
        return round(($this->radius ** 2) * pi(), 4);
    }

    public function getPerimeter(): int|float
    {
        return round(2 * pi() * $this->radius, 4);
    }
}

try {
    $Rectangle = new Rectangle(5, 3);

} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}
echo "Rectangle width: " . $Rectangle->GetterWidth() . PHP_EOL;
echo "Rectangle heigh: " . $Rectangle->GetterHeigh() . PHP_EOL;
echo "Area rectangle: " . $Rectangle->getArea() . PHP_EOL;
echo "Perimeter rectangle: " . $Rectangle->getPerimeter() . PHP_EOL;

try {
    $Circle = new Circle(-5);
    echo "Radius: " . $Circle->Getter() . PHP_EOL;
    echo "Area circle: " . $Circle->getArea() . PHP_EOL;
    echo "Perimeter circle: " . $Circle->getPerimeter() . PHP_EOL;
} catch (Exception $error) {
    echo $error->getMessage();
    exit;
}

