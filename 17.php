<?php

abstract class Figure
{

    abstract public function getArea(): int|float;

    abstract public function getPerimeter(): int|float;
}

class Rectangle extends Figure
{
    public int|float $width;
    public int|float $heigh;

    public function __construct($width, $heigh)
    {
        $this->width = $width;
        $this->heigh = $heigh;
        if ($this->width <= 0 or $this->heigh <= 0) {
            exit('Value can not be less then zero' . PHP_EOL);
        }
    }

    public function getArea(): int|float
    {
        return $this->heigh * $this->width;
    }

    public function getPerimeter(): int|float
    {
        return $this->heigh * 2 + $this->width * 2;
    }
}

class Circle extends Figure
{
    public int|float $radius;

    public function __construct($radius)
    {
        $this->radius = $radius;

        if ($this->radius <= 0) {
            exit('Value can not be less then zero' . PHP_EOL);
        }
    }

    public function getArea(): int|float
    {
        return round(($this->radius ** 2) * pi(), 4);
    }

    public function getPerimeter(): int|float
    {
        return round(2 * pi() * $this->radius, 4);
    }
}

$Rectangle = new Rectangle(10, 3.1);
echo "Width: ".$Rectangle->width.PHP_EOL;
echo "Heigh: ".$Rectangle->heigh.PHP_EOL;
echo "Area rectangle: " . $Rectangle->getArea() . PHP_EOL;
echo "Perimeter rectangle: " . $Rectangle->getPerimeter() . PHP_EOL;

$Circle = new Circle(5);
echo "Radius: ".$Circle->radius.PHP_EOL;
echo "Area circle: " . $Circle->getArea() . PHP_EOL;
echo "Perimeter circle: " . $Circle->getPerimeter() . PHP_EOL;