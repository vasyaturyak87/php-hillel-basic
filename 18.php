<?php

trait CalcCost
{
    public $Mileage;

    //cost of petrol 1.5$ per litter
    private static $FuelCost = 1.5;

    public function Coefficient(int $Mileage):int
    {
        return ($Mileage * self::$FuelCost) / 100;
    }
}

class Volvo
{

    use CalcCost;
    //AverageConsumption fuel on 100 km
    private $AverageConsumption = 10;

    public function Cost($Mileage)
    {
        echo "Fuel cost for Volvo on $Mileage km equal: " . $this->Coefficient($Mileage) * $this->AverageConsumption . "$" . PHP_EOL;
    }

}

$VolvoFuelCost = new Volvo();
$VolvoFuelCost->Cost(20000);
