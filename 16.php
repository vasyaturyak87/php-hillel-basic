<?php

class Text
{
    public string $_text = "some text";

    public function print()
    {
        echo ucfirst($this->_text) . PHP_EOL;
    }
}

$Test = new Text();
$Test->print();

class UpperText extends Text
{
    public function print()
    {
        echo strtoupper($this->_text) . PHP_EOL;
    }
}

$_ToUpper = new UpperText();
$_ToUpper->print();