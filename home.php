<?php
/*echo "Вітаємо!" .PHP_EOL;
echo "Введіть будь-ласка своє і'мя:" .PHP_EOL;
$_NameInput = fgets(STDIN);
echo "Введіть будь-ласка поточний час в годинах:" .PHP_EOL;
$_Hours = fgets(STDIN);
//$_Hours = date("h")+3;
//+3 correction on Kiev time
if ($_Hours>6 && $_Hours<12){
    echo "Доброго ранку $_NameInput";
} elseif($_Hours>12 && $_Hours<17){
    echo "Доброго Дня $_NameInput";
} elseif($_Hours>17 && $_Hours<22){
    echo "Доброго Вечора $_NameInput";
} else{
    echo "Доброї ночі $_NameInput";
}

echo "Введіть будь-ласка чотири рандомних числа і ми вам покажемо магію )))" .PHP_EOL;
echo "Введіть число №1:" .PHP_EOL;
$_Namber1 = fgets(STDIN);
echo "Введіть число №2:" .PHP_EOL;
$_Namber2 = fgets(STDIN);
echo "Введіть число №3:" .PHP_EOL;
$_Namber3 = fgets(STDIN);
echo "Введіть число №4:" .PHP_EOL;
$_Namber4 = fgets(STDIN);
$_MyArray=array($_Namber1,$_Namber2,$_Namber3,$_Namber4);
echo "Загальна сума чисел дорівнює: " . array_sum($_MyArray) .PHP_EOL;
echo "Середнє арифметичне дорівнює: " . ((round((array_sum($_MyArray)),5))/4) .PHP_EOL;

$_VariablesOne=2;
if(isset($_VariablesOne)){
    echo "Variable One is set." .PHP_EOL;
    echo "Value equal $_VariablesOne" .PHP_EOL;
} else{
    echo 'Variable One isn\'t set.'.PHP_EOL;
};


$_VariablesTwo;

echo (isset($_VariablesTwo))? "Variable Two is set: " . $_VariablesTwo .PHP_EOL: "Variable Two isn't set.".PHP_EOL;

echo "Введіть будь-ласка будь-який номер і ми вам покажемо магію як числа перетворюються на колір )))" .PHP_EOL;
echo "Ваше число: " .PHP_EOL;
$_Value = fgets(STDIN);
switch ($_Value) {
    case 1:
        echo "Ваш колір: Green" .PHP_EOL;
        break;
    case 2:
        echo "Ваш колір: Red" .PHP_EOL;
        break;
    case 3:
        echo "Ваш колір: Blue" .PHP_EOL;
        break;
    case 4:
        echo "Ваш колір: Brown" .PHP_EOL;
        break;
    case 5:
        echo "Ваш колір: Violet" .PHP_EOL;
        break;
    case 6:
        echo "Ваш колір: Black" .PHP_EOL;
        break;
       default:
        echo "Ваш колір: White" .PHP_EOL;
}

echo 'SCRIPT ONE' . PHP_EOL;
$_Value = 5;
function test(&$n)
{
    $n *= $n;
}

test($_Value);
print "Sqare of 5 is: " . $_Value . PHP_EOL;

echo 'SCRIPT TWO' . PHP_EOL;
$_Text = "Hi";
function text(&$x)
{
    $x .= ' Vasya';
}

text($_Text);
print $_Text;

echo 'SCRIPT ONE - Square of circle' . PHP_EOL;
echo 'Enter radius circle in meters' . PHP_EOL;
$_CircleRadius = fgets(STDIN);
function Squre(int|float $x):int|float|string
{
    return "Square of circle of $x meters is " . round($x * $x * pi(), 2) . ' sqr.m.';
}

print Squre($_CircleRadius);

echo 'SCRIPT THREE - By Referance and without' . PHP_EOL;
$_Number = 5;
$_Power = 3;
function ToPower($n, $x)
{
    return $n ** $x;
}
print "Number $_Number" . PHP_EOL;
print "Power $_Power" . PHP_EOL;
print "Without referance:" . ToPower($_Number, $_Power) . PHP_EOL;

$_Number2 = 5;
$_Power2 = 3;
function NumberRef($n)
{
   return $n += 20;
}
NumberRef($_Number2);
function PowerRef($n)
{
   return $n += 4;
}
PowerRef($_Power2);

print "Number $_Number2" . PHP_EOL;
print "Power $_Power2" . PHP_EOL;

print "With reference, number $_Number2 to power $_Power2 equal:" . $_Number2*$_Power2;

$_Float =5.5; $_Integer= 5; $_StringN='5.5'; $_StringT='Hello'; $_StringNandT='111100.555Day';
$_BooleanT=true;$_BooleanF=false;$_Null=null;$_Empty='';
//comparison
var_dump($_Float==$_StringN);
var_dump($_Float===$_StringN);
var_dump($_StringN==$_StringT);
var_dump($_StringN===$_StringT);
var_dump($_Null==$_Empty);
var_dump($_Null===$_Empty);
var_dump($_BooleanT or $_BooleanF);
var_dump($_BooleanT or $_BooleanT);
var_dump($_BooleanT xor $_BooleanF);
var_dump($_BooleanT xor $_BooleanT);

$_Float =5.5; $_Integer= 5; $_StringN='5.5'; $_StringT='Hello'; $_StringNandT='111100.555Day';
$_BooleanT=true;$_BooleanF=false;$_Null=null;$_Empty='';
//conversion
$_FloatNew = intval($_Float).PHP_EOL;
$_StringN_New = intval($_StringN).PHP_EOL;
$_EmptyNew = intval($_Empty).PHP_EOL;
$_StringNandT_NewFl = floatval($_StringNandT).PHP_EOL;
$_StringNandT_NewInt = intval($_StringNandT).PHP_EOL;
$_Empty_New=intval($_Empty);
//comparison
var_dump($_FloatNew==$_StringN_New);
var_dump($_FloatNew===$_StringN_New);
var_dump($_StringN_New==$_StringT);
var_dump($_StringN_New===$_StringT);
var_dump($_Null==$_Empty);
var_dump($_Null===$_Empty);
var_dump($_Null==$_Empty_New);
var_dump($_Null===$_Empty_New);

$_Test = array();
for ($i = 0; $i < 5; $i++) {
    $_Test[$i]=rand(-100,100);
}
echo 'Unsorted array:' . PHP_EOL;
for ($i = 0; $i < 5; $i++) {
    echo $_Test[$i] . PHP_EOL;
}
$_SortArray = $_Test;
sort($_SortArray);
echo 'Ascending sorted array:' . PHP_EOL;
for ($i = 0; $i < 5; $i++) {
    echo $_SortArray[$i] . PHP_EOL;
}
$_Max = 0;
foreach ($_Test as $_Key => $_Value) {
    if ($_Max < $_Value) {
        $_Max = $_Value;
    }
}
$_Min = 0;
foreach ($_Test as $_Key => $_Value) {
    if ($_Min > $_Value) {
        $_Min = $_Value;
    }
}
echo 'Max fn ForEach:' . $_Max . PHP_EOL;
echo 'Min fn ForEach:' . $_Min . PHP_EOL;
echo 'Max of array: ' . max($_Test) . PHP_EOL;
echo 'Min of array: ' . min($_Test) . PHP_EOL;

echo 'Closure' . PHP_EOL;
$function = function ($b) {
    echo $b. PHP_EOL;
};
function myFunction(int $value1, int $value2, $function)
{
    $result = $value1 * $value2;
    if (isset($function)) {
        $function($result);}
    return $result .PHP_EOL;
    };

myFunction(5, 5, $function) . PHP_EOL;
//echo $result=myFunction(5,5);
*/
//
//$_TestString = 'myteststring';
//function StringToArray($_X)
//{
//    $_StringLength = strlen($_X);
//    for ($i = 0; $i < $_StringLength; $i++) {
//        echo $_X[i];
//    }
//}
//
//StringToArray($_TestString);
//$m = memory_get_peak_usage();
//function generator(int $_End): Generator
//{
//    $_First = 0;
//    yield $_First;
//    $_Second = 1;
//    $_Sum = 0;
//    while($_Sum <= $_End) {
//        yield $_Sum;
//        $_Sum = $_First + $_Second;
//        $_First = $_Second;
//        $_Second = $_Sum;
//    }
//}
//
//echo "Fibonacci sequence:" . PHP_EOL;
//foreach (generator(20) as $val) {
//    echo $val . PHP_EOL;
//}
//echo "Memory usage:" . memory_get_peak_usage() - $m . PHP_EOL;

//$_File=fopen('test.txt','w');
//
//echo "Enter some text in FIRST line:";
//$_FirstLine ="First line: " . date("h:i:sa"). ' ' . fgets(STDIN);
//fwrite($_File, $_FirstLine);
//
//echo "Enter some text in SECOND line:";
//$_SecondLine ="Second line: " . date("h:i:sa"). ' ' . fgets(STDIN);
//fwrite($_File, $_SecondLine);
//
//echo "Enter some text in THIRD line:";
//$_FirstLine ="Third line: " . date("h:i:sa"). ' ' . fgets(STDIN);
//fwrite($_File, $_FirstLine);
//
//fclose($_File);

//$_File=fopen('test.txt','r');
//while($_line=fgets($_File)){
//    echo $_line;
//}
//fclose($_File);
//enum status
//{
//    case completed;
//    case unfinished;
//}
//
//class ListOfTasks
//{
//    //property
//    public $taskId;
//    public $taskName;
//    public $priority;
//
//    //methods
//    public function addTask()
//    {
//        $filePath = 'TaskList.txt';
//        $line = count(file($filePath)) + 1;
//        $test = "ID: " . $line . ". Priority: " . $this->priority . '. Task: ' . $this->taskName . '.' . PHP_EOL;
//        $_File = fopen('TaskList.txt', 'a');
//        fwrite($_File, $test);
//        fclose($_File);
//    }
//
//    public function getTasks()
//    {
//        $_File = fopen('TaskList.txt', 'r');
//        while ($_line = fgets($_File)) {
//            echo $_line;
//        }
//        fclose($_File);
//    }
////deleteTask($taskId)
////completeTask($taskId)
//}
//
//$TaskOne = new ListOfTasks;
//$TaskOne->priority = 'High';
//$TaskOne->taskName = 'Eat something';
//
//$TaskTwo = new ListOfTasks;
//$TaskTwo->priority = 'Medium';
//$TaskTwo->taskName = 'Clean flat';
//
//$TaskThree = new ListOfTasks;
//$TaskThree->priority = 'Low';
//$TaskThree->taskName = 'watch TV';
//
//echo $TaskOne->addTask() . PHP_EOL;
//echo $TaskOne->getTasks() . PHP_EOL;
//
//echo $TaskTwo->addTask() . PHP_EOL;
//echo $TaskTwo->getTasks() . PHP_EOL;
//
//echo $TaskThree->addTask() . PHP_EOL;
//echo $TaskThree->getTasks() . PHP_EOL;

//echo phpinfo();