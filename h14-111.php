<?php

enum status
{
    case completed;
    case unfinished;
}

class ListOfTasks
{
    //property
    public int $taskId;
    public int $taskIdStatus;
    public string $taskName;
    public string $priority;
    private array $Array;

    public function __construct()
    {
        echo "Start Class" . PHP_EOL;
        $_File = fopen('TaskList.txt', 'r');
        $_OpenArray = array();
        while ($_line = fgets($_File)) {
            $_OpenArray[] = $_line;
        }
        $this->Array = $_OpenArray;
        fclose($_File);
    }

    //methods
    public function addTask(string $taskName, string $priority): void
    {
        $this->taskName = $taskName;
        $this->priority = $priority;
//search max ID
        $_ArrayId = array();
        foreach ($this->Array as $value) {
            preg_match_all('!\d+!', $value, $matches);
            $_ArrayId[] = $matches[0][0];
        }

        $_Max = 0;
        foreach ($_ArrayId as $value) {
            if ($value > $_Max) {
                $_Max = $value;
            }
        }
        $_AddTask = "ID: " . ($_Max + 1) . ". Priority: " . $this->priority . " " .
            "status::unfinished" . '. Task: ' . $this->taskName . '.' . PHP_EOL;
        $this->Array[] = $_AddTask;

    }

    public function getTasks(): void
    {
        $_High = array();
        $_Medium = array();
        $_Low = array();
        foreach ($this->Array as $value) {
            if (strpos($value, ": High")) {
                $_High[] = $value;
            }
            if (strpos($value, ": Medium")) {
                $_Medium[] = $value;
            }
            if (strpos($value, ": Low")) {
                $_Low[] = $value;
            }
        }
        $SortedArray = array_merge($_High, $_Medium, $_Low);
        $this->Array = $SortedArray;
    }

    public function deleteTask(int $taskId): void
    {
        $Del = array();
        $this->taskId = $taskId;
        foreach ($this->Array as $value) {
            if (!strpos($value, "D: $this->taskId")) {
                $Del[] = $value;
            }
        }
        $this->Array = $Del;
    }

    public function completeTask(int $taskIdStatus): void
    {
        echo "Change Task Status" . PHP_EOL;
        $this->taskIdStatus = $taskIdStatus;
        $_Com = array();
        foreach ($this->Array as $value) {
            if (strpos($value, "D: $this->taskIdStatus")) {
                $value = str_replace("status::unfinished", "status::completed", $value);
            }
            $_Com[] = $value;
        }
        $this->Array = $_Com;
    }

    function __destruct()
    {
        $_FileNew = fopen('TaskList.txt', 'w');
        $Length = count($this->Array);
        for ($i = 0; $i < $Length; $i++) {
            fwrite($_FileNew, $this->Array[$i]);
        }
        echo "Finish Class" . PHP_EOL;
    }
}

$TaskOne = new ListOfTasks();
//$TaskOne->addTask('Spend time with friends', 'Medium');
//$TaskOne->getTasks();
//$TaskOne->deleteTask(1);
$TaskOne->completeTask(6);




