<?php
interface Figure
{

    public function getArea(): int|float;

    public function getPerimeter(): int|float;
}
