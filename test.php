<?php

trait TotalCost
{
    public static $name;
    public static $AverageConsumption;
    public static $Mileage;

    //cost of petrol 1.5$ per litter
    private static $FuelCost = 1.5;

    public function Cost()
    {
        echo "Fuel cost for " . self::$name . " on " . self::$Mileage . " km equal:" .
            (self::$Mileage * self::$AverageConsumption * self::$FuelCost) / 100 . "$" . PHP_EOL;
    }
}

class Volvo
{
    use TotalCost;
}

class BMW
{

    use TotalCost;
}


$VolvoFuelCost = new Volvo();
Volvo::$name = 'Volvo';
Volvo::$AverageConsumption = 15;
Volvo::$Mileage = 200;
$VolvoFuelCost->Cost();

$BmwFuelCost = new BMW();
BMW::$name = 'BMW';
BMW::$AverageConsumption = 10;
BMW::$Mileage = 200;
$BmwFuelCost->Cost();