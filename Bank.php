<?php

class BankAcount
{
//property

    private int $_Balance;
    private int $_AcountNumber;

    private $_DailyAmount = 500;


    public function __construct()
    {
        echo "Start working with acount" . PHP_EOL;
    }

    //methods
    public function SetBalance(int $_Balance)
    {
        $this->_Balance = $_Balance;
    }

    public function SetAcountNumber(int $_AcountNumber)
    {
        $this->_AcountNumber = $_AcountNumber;
    }

    public function GetBalance(): int
    {
        return $this->_Balance;
    }

    public function GetAcountNumber(): int
    {
        return $this->_AcountNumber;
    }

    private function ErrorHandler($_WithdrawalSum, $_TopUpAmount)
    {
        try {
            $this->Withdrawal($_WithdrawalSum);
            $this->TopUp($_TopUpAmount);
        } catch (Exception $exception) {
            echo $exception->getMessage();
            exit;
        }

    }

    /**
     * @throws Exception
     */
    public function Withdrawal(int $_WithdrawalSum)
    {
        if ($_WithdrawalSum > $this->_Balance) {
            throw new Exception("Sum of withdrawal exeed your balance. Your balance $this->_Balance. ");
        };
        $this->_Balance -= $_WithdrawalSum;
        echo "Now your balance is $this->_Balance" . PHP_EOL;
    }

    /**
     * @throws Exception
     */
    public function TopUp(int $_TopUpAmount)
    {
        if ($_TopUpAmount > $this->_DailyAmount) {
            throw new Exception("You exeed daily amount of TopUp. Reduce sum to: $this->_DailyAmount and try again");
        }
        $this->_Balance += $_TopUpAmount;
        echo "Now your balance is $this->_Balance" . PHP_EOL;
    }

    public function __destruct()
    {
        echo "Finish" . PHP_EOL;
    }
}