<?php

enum status
{
    case completed;
    case unfinished;
}

class ListOfTasks
{
    //property
    public string $FilePath;
    public int $taskId;
    public int $taskIdStatus;
    public string $taskName;
    public string $priority;
    private array $Array;

    public function __construct($FilePath)
    {
        $this->FilePath = $FilePath;
        echo "Start Class" . PHP_EOL;
        $this->OpenReadFile();
    }

    //methods

    private function OpenReadFile()
    {
        $_File = fopen($this->FilePath, 'a+');
        $_OpenArray = array();
        while ($_line = fgets($_File)) {
            $_OpenArray[] = $_line;
        }
        $this->Array = $_OpenArray;
        fclose($_File);
    }

    public function addTask(string $taskName, string $priority): void
    {
        $this->taskName = $taskName;
        $this->priority = $priority;
//search max ID
        $_ArrayId = array();
        foreach ($this->Array as $value) {
            preg_match_all('!\d+!', $value, $matches);
            $_ArrayId[] = $matches[0][0];
        }

        $_Max;
        if (empty($_ArrayId)) {
            $_Max = 0;
        } else {
            $_Max = max($_ArrayId);
        };

        $_AddTask = "ID: " . ($_Max + 1) . ". Priority: " . $this->priority . " " .
            status::unfinished . '. Task: ' . $this->taskName . '.' . PHP_EOL;
        $this->Array[] = $_AddTask;

    }

    public function getTasks(): void
    {
        $_High = array();
        $_Medium = array();
        $_Low = array();
        foreach ($this->Array as $value) {
            if (strpos($value, ": High")) {
                $_High[] = $value;
            }
            if (strpos($value, ": Medium")) {
                $_Medium[] = $value;
            }
            if (strpos($value, ": Low")) {
                $_Low[] = $value;
            }
        }
        $SortedArray = array_merge($_High, $_Medium, $_Low);
        $this->Array = $SortedArray;
    }

    public function deleteTask(int|bool $taskId): void
    {
        $Del = array();
        $this->taskId = $taskId;
        foreach ($this->Array as $value) {
            if (strpos($value, "D: $this->taskId") === false) {
                $Del[] = $value;
            }
        }
        $this->Array = $Del;
    }

    public function completeTask(int $taskIdStatus): void
    {
        echo "Change Task Status" . PHP_EOL;
        $this->taskIdStatus = $taskIdStatus;
        $_Com = array();
        foreach ($this->Array as $value) {
            if (strpos($value, "D: $this->taskIdStatus")) {
                $value = str_replace("status::unfinished", "status::completed", $value);
            }
            $_Com[] = $value;
        }
        $this->Array = $_Com;
    }

    function __destruct()
    {
        $_FileNew = fopen($this->FilePath, 'w');
        $Length = count($this->Array);
        for ($i = 0; $i < $Length; $i++) {
            fwrite($_FileNew, $this->Array[$i]);
        }
        echo "Finish Class" . PHP_EOL;
    }
}

$TaskOne = new ListOfTasks('TaskList.txt');
//$TaskOne->addTask('Drink water', 'Medium');
//$TaskOne->getTasks(4);
$TaskOne->deleteTask(4);
//$TaskOne->completeTask();




