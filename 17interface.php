<?php

interface Figure
{
    public function getArea(): int|float;

    public function getPerimeter(): int|float;
}


class Rectangle implements Figure
{
    public int|float $width;
    public int|float $heigh;

    public function __construct(int|float $width, int|float $heigh)
    {
                try {
            $this->Setter($width, $heigh);
        } catch (Exception $exception) {
            echo $exception->getMessage();
            exit;
        }
    }

    /**
     * @throws Exception
     */
    public function Setter(int|float $width, int|float $heigh): void
    {

        if ($width <= 0 or $heigh <= 0) {
            throw new Exception('Value width or heigh can not be less then zero or equal to zero');
        }
        $this->width = $width;
        $this->heigh = $heigh;
    }

    public function GetterWidth(): int|float
    {
        return $this->width;
    }

    public function GetterHeigh(): int|float
    {
        return $this->heigh;
    }

    public function getArea(): int|float
    {
        return $this->heigh * $this->width;
    }

    public function getPerimeter(): int|float
    {
        return $this->heigh * 2 + $this->width * 2;
    }
}

class Circle implements Figure
{
    public int|float $radius;

    public function __construct(int|float $radius)
    {
        $this->Setter($radius);

        try {
            $this->Setter($radius);
        } catch (Exception $exception) {
            echo $exception->getMessage();
            exit;
        }
    }

    /**
     * @throws Exception
     */
    public function Setter(int|float $radius): void
    {

        if ($radius <= 0) {
            throw new Exception('Value can not be less then zero or equal to zero');
        }
        $this->radius = $radius;
    }

    public function Getter(): int|float
    {
        return $this->radius;
    }

    public function getArea(): int|float
    {
        return round(($this->radius ** 2) * pi(), 4);
    }

    public function getPerimeter(): int|float
    {
        return round(2 * pi() * $this->radius, 4);
    }
}

$Rectangle = new Rectangle(50, 33.3);
echo "Rectangle width: " . $Rectangle->GetterWidth() . PHP_EOL;
echo "Rectangle heigh: " . $Rectangle->GetterHeigh() . PHP_EOL;
echo "Area rectangle: " . $Rectangle->getArea() . PHP_EOL;
echo "Perimeter rectangle: " . $Rectangle->getPerimeter() . PHP_EOL;

$Circle = new Circle(5.3);
echo "Radius: " . $Circle->Getter() . PHP_EOL;
echo "Area circle: " . $Circle->getArea() . PHP_EOL;
echo "Perimeter circle: " . $Circle->getPerimeter() . PHP_EOL;